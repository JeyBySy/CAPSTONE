const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");
const cookieSession = require("cookie-session");
const passport = require("passport");
const mongoose = require("mongoose");
const Pusher = require("pusher");
require("dotenv").config();

let port = process.env.PORT || 8080;

//For Socket.io import
// const io = require('socket.io')(server);

//Pusher connection
// const pusher = new Pusher({
//   appId: "1248087",
//   key: "39cc216c13dd88ef22ea",
//   secret: "246a467c5d7e470eb7bf",
//   cluster: "ap1",
//   useTLS: true,
// });

//Router exports
require("./public/js/google-oauth");
const _pusher = require("./router/pusher");
const addNewForm = require("./router/add-new-form");
const showForm = require("./router/show-form-created");
const alumni = require("./router/alumni-page");

app.use(cors());
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use("/public", express.static(path.join(__dirname, "/public")));
app.use("/form", addNewForm);
app.use("/form/new", _pusher);
app.use("/form", showForm);
app.use("/", alumni);

//Model router import
const createForm = require("./models/Create-Form-Model");
const User = require("./models/New-User-Model");

//Mongodb connection (put mongodb_uri to .env file)
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Connected to db");
  })
  .catch((e) => {
    console.log("No Connection " + e);
  });

//Cookies setup
app.use(
  cookieSession({
    name: "session",
    keys: ["key1", "key2"],
  })
);

// If not registered then go to the landing page
const isLogin = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    return res.redirect("/");
  }
};

// If registered then go to the admin page
const isNotLogin = (req, res, next) => {
  if (req.user) {
    return res.redirect("/dashboard");
  }
  next();
};
app.use(passport.initialize());
app.use(passport.session());

// //Render Landing Page
app.get("/", isNotLogin, (req, res) => {
  res.render("index", { layout: false });
});

//Render dashboard page
app.get("/dashboard", isLogin, async (req, res) => {
  var googleIcon = req.user.photos[0].value;
  var sampleID = [
    {
      id: "qwert",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: [
        "newdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd",
        "awit",
        "ahaha",
        "lol",
      ],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "Awit",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "asdfs",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "fasdf",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "qwerfdshgbt",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "gdsgew",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "gewhgb",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
    {
      id: "qwgberert",
      title: "Graduate Tracing 2020-2021dsadsadsadsadasdsadsa sd sadsadsad",
      answer: ["new", "awit", "ahaha", "lol"],
      data: ["10", "20", "30", "40"],
    },
  ];
  var total = sampleID.length;

  return res.render("dashboard", {
    googleIcon: googleIcon,
    sampleID,
    total,
  });
});

//Render form page
app.get("/form", isLogin, async (req, res) => {
  var googleIcon = req.user.photos[0].value;
  var id = await User.findOne({ "usr.sub": req.user.id });
  return res.render("form", {
    googleIcon: googleIcon,
    id: id._id,
    data: id.forms,
  });

  //!!!NEEDED!!!
  // await createForm.create({
  //   category:"helloo"
  // },(error, newForm)=>{
  //     if(error){
  //     console.log(error)
  //   }
  //   console.log(newForm)
  //   return res.render('form',{
  //     googleIcon:googleIcon
  // })
  // })
});

//Render form/new page
app.get("/form/new/:id", isLogin, async (req, res) => {
  var query_id = req.params.id;
  var googleIcon = req.user.photos[0].value;
  var data = await User.findOne({ "usr.sub": req.user.id });
  //  console.log(data._id)
  return res.render("newForm", {
    googleIcon: googleIcon,
    query_id,
    userID: data._id,
    data: data.forms,
  });
});

//Render form/recent page
app.get("/form/recent/:id", isLogin, async (req, res) => {
  var query_id = req.params.id;
  var googleIcon = req.user.photos[0].value;
  var data = await User.findOne({ "usr.sub": req.user.id });

  return res.render("newForm", {
    googleIcon: googleIcon,
    query_id,
    userID: data._id,
    data: data.forms,
  });
});

//Render event page
app.get("/event", isLogin, (req, res) => {
  var googleIcon = req.user.photos[0].value;
  return res.render("event", {
    googleIcon: googleIcon,
  });
});

//render Alumni page
app.get("/alumni", isLogin, (req, res) => {
  var googleIcon = req.user.photos[0].value;
  return res.render("alumni", {
    googleIcon: googleIcon,
  });
});

//render Bin page
app.get("/bin", isLogin, (req, res) => {
  var googleIcon = req.user.photos[0].value;
  return res.render("bin", {
    googleIcon: googleIcon,
  });
});

//Render Downloadables
app.get("/downloadables", isLogin, (req, res) => {
  var googleIcon = req.user.photos[0].value;
  return res.render("download", {
    googleIcon: googleIcon,
  });
});

//Google sign in setup
app.get(
  "/google",
  isNotLogin,
  passport.authenticate("google", { scope: ["profile", "email"] })
);

//Rendering page after google sign-in
app.get(
  "/google/callback",
  isNotLogin,
  passport.authenticate("google", { failureRedirect: "/" }),
  async (req, res) => {
    // console.log(req.user)
    await User.findOne({ "usr.sub": req.user.id }, async function (err, user) {
      if (err) {
        console.log(err);
        return res.redirect("/");
      }
      if (!user) {
        await User.create(
          {
            // sub:req.user.id,
            usr: req.user._json,
            forms: [],
          },
          (error, newUser) => {
            if (error) {
              console.log(error);
            }
            console.log(newUser);
          }
        );
        return res.redirect("/dashboard");
        // return res.redirect('/')
      }
      return res.redirect("/dashboard");
    });
  }
);
app.get("/logout", (req, res) => {
  req.session = null;
  req.logout();
  res.clearCookie("session.sig", { path: "/" });
  res.redirect("/");
});

app.use(function (req, res, next) {
  res.status(404).render("404");
});

// Port
app.listen(port, () => {
  console.log(`Listen to ports ${port}`);
});

//!! IMPORTANT !!

// io.on('connection', function(socket) {
//    console.log('A user connected');

//    //Whenever someone disconnects this piece of code executed
//    socket.on('disconnect', function () {
//       console.log('A user disconnected');
//    });
// });
