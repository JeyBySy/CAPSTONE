const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = require("../models/New-User-Model");

router.get("/jherico-cocosa", async (req, res) => {
  const sample = await User.find({});
  console.log(sample);
  return res.render("alumni-main");
});

module.exports = router;
