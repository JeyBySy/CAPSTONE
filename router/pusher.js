const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Pusher = require("pusher");

const User = require("../models/New-User-Model");

const pusher = new Pusher({
  appId: process.env.APP_ID,
  key: process.env.KEY,
  secret: process.env.SECRET,
  cluster: "ap1",
  useTLS: true,
});

//  pusher.trigger("my-channel", "my-event", {
//         message: "helfdddddffdlo"
//     });

router.post("/realtime-data/:formID/:userID", async (req, res) => {
  const userID = req.params.userID;
  const formID = req.params.formID;
  // console.log(userID + " " + formID);
  await User.findOneAndUpdate(
    {
      _id: userID,
      forms: { $elemMatch: { id: formID } },
    },
    {
      $set: {
        "forms.$.title": req.body.title || "Untitle Form",
      },
    },
    {
      new: true,
      upsert: true,
    },
    (error, result) => {
      if (error) {
        console.log(error);
      }
      pusher.trigger("my-channel", "my-event", {
        message: "helfddddsdffdlo",
      });
      return res.redirect(`/form/recent/${formID}`);
      // return res.json({ success: true, message: "ok" });
    }
  );
});

module.exports = router;
