const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const axios = require("axios");

const User = require("../models/New-User-Model");

router.post("/:id", async (req, res) => {
  const id = req.params.id;
  const form_id = String(new mongoose.mongo.ObjectID());
  User.findOneAndUpdate(
    {
      _id: id,
    },
    {
      $push: {
        forms: {
          id: form_id,
          title: "Untitled Form",
          questions: [
            {
              title: "what is the correct number",
              type: "radio",
              answer: [
                {
                  value: "100",
                },
                {
                  value: "300",
                },
              ],
            },
          ],
          response: [
            {
              name: "jehrico",
            },
          ],
        },
      },
    },
    (error, result) => {
      if (error) {
        console.log(error);
      }
      return res.redirect(`/form/new/${form_id}`);
    }
  );
});

module.exports = router;
