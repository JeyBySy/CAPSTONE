const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require('../../models/New-User-Model.js')

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new GoogleStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret:process.env.CLIENT_SECRET,
    callbackURL: "http://localhost:8080/google/callback" //"https://sample-formiva.herokuapp.com/google/callback" 
  },
  async function(accessToken, refreshToken, profile, done) {
    return done(null, profile);

  }
));