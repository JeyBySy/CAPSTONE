const toggleBtn = document.getElementById("themeBtnID") 
const closeBTN = document.getElementById("close-btn")


 const currentTheme = localStorage.getItem("theme")
    if(currentTheme === "dark"){
        //  toggleBtn.textContent= "Light Mode"
        document.documentElement.setAttribute("data-theme", currentTheme);
        closeBTN.classList.add("btn-close-white")
    }
     if(currentTheme === "light"){
        //  toggleBtn.textContent= "Dark Mode"
        document.documentElement.setAttribute("data-theme", currentTheme);
        
    }
toggleBtn.addEventListener("click",()=>{
    document.getElementById("overlay").style.display = "none";
    document.getElementById("sideNavID").style.left = "-100%";

    let isToggle = localStorage.getItem("theme") === "light";
   

    switch(isToggle){
        case true:
            localStorage.setItem("theme", "dark")
            closeBTN.classList.add("btn-close-white")
            document.documentElement.setAttribute("data-theme", "dark");
            document.documentElement.style.transition = "ease-in-out 0.3s"
            // toggleBtn.textContent= "Light Mode"
            break
        case false:
             localStorage.setItem("theme", "light")
             closeBTN.classList.remove("btn-close-white")
             document.documentElement.setAttribute("data-theme", null)
             document.body.style.transition = "ease-in-out 0.3s"
            //  toggleBtn.textContent= "Dark Mode"
            break

    }
    
})

 
 

