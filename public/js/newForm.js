// Enable pusher logging - don't include this in production
// Pusher.logToConsole = true;

// var pusher = new Pusher("39cc216c13dd88ef22ea", {
//   cluster: "ap1",
// });

// var channel = pusher.subscribe("my-channel");
// channel.bind("my-event", function (data) {
//   title;
// });

document.addEventListener("DOMContentLoaded", () => {
  const txt = document.getElementById("question-item");
  const radio = document.getElementById("widget-radio");
  const baseURL = "https://ghibliapi.herokuapp.com/films";

  radio.addEventListener("click", function () {
    fetchData();
  });
  function fetchData(baseURL) {
    fetch()
      .then((response) => response.json())
      .then((data) => renderData(data));
  }

  function renderData(data) {
    for (const q of data) {
      console.log(q.title);
      const text = document.createTextNode(q.title);
      txt.appendChild(text);
    }
  }
});
