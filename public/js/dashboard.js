const nextEventBTN = document.getElementById("arrow-to-eventID");
const responseTab = document.getElementById("responseDashID");
const eventTab = document.getElementById("eventDashID");
responseTab.style.transition = "ease-in-out 0.6s";
eventTab.style.transition = "ease-in-out 0.6s";
var isShown = true;
nextEventBTN.addEventListener("click", () => {
  document.getElementById("overlay").style.display = "none";
  document.getElementById("sideNavID").style.left = "-100%";
  switch (isShown) {
    case true:
      responseTab.style.left = "-100%";
      eventTab.style.right = "0";
      nextEventBTN.textContent = "RESPONSE";
      isShown = false;
      break;

    case false:
      responseTab.style.left = "0";
      eventTab.style.right = "-100%";
      nextEventBTN.textContent = "EVENT";
      isShown = true;
      break;
  }
});

// ---------------------------------------------TABLES----------------------------
$(document).ready(function () {
  $("#display-table").DataTable({
    paging: false,
    scrollY: "35vh",
    scrollX: true,
    responsive: true,
  });
  $(".dataTables_length").addClass("bs-select");
});

var tooltipTriggerList = [].slice.call(
  document.querySelectorAll('[data-bs-toggle="tooltip"]')
);
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});
